package com.tdd;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.fail;

@RunWith(JUnit4.class)
public class DelimiterTest {

    private Delimiter delimiter;

    @Test
    public void withoutDelimiterDefinitionCharacter() {
        try {
            new Delimiter("1,2").getDelimiterRegExpString();
            fail();
        } catch (RuntimeException e) {
            assertThat(e.getMessage()).isEqualTo("Delimiter Definition Char Sequence \"//\" not found");
        }
    }

    @Test
    public void singleLetterDelimiter() {
        delimiter = new Delimiter("//;");
        assertThat(delimiter.getDelimiterRegExpString()).isEqualTo(";");
    }

    @Test
    public void multiLetterDelimiter() {
        delimiter = new Delimiter("//[%%]");
        assertThat(delimiter.getDelimiterRegExpString()).isEqualTo("%%");
    }

    @Test
    public void multipleSingleLetterDelimiter() {
        delimiter = new Delimiter("//[%][-]");
        assertThat(delimiter.getDelimiterRegExpString()).isEqualTo("%|-");
    }

    @Test
    public void multipleMultiLetterDelimiter() {
        delimiter = new Delimiter("//[%%][--]");
        assertThat(delimiter.getDelimiterRegExpString()).isEqualTo("%%|--");
    }

}