package com.tdd;

import java.util.List;

class NegativeNumberException extends RuntimeException {
    private List<Integer> negativeNumbers;

    NegativeNumberException(List<Integer> negativeNumbers) {
        this.negativeNumbers = negativeNumbers;
    }

    List<Integer> getNegativeNumbers() {
        return negativeNumbers;
    }
}
