package com.tdd;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.fail;

@RunWith(JUnit4.class)
public class StringCalculatorTest {

    private StringCalculator calculator;

    @Before
    public void init() {
        calculator = new StringCalculator();
    }

    @Test
    public void emptyString() {
        assertThat(calculator.sum("")).isEqualTo(0);
    }

    @Test
    public void singleNumber() {
        assertThat(calculator.sum("5")).isEqualTo(5);
    }

    @Test
    public void commaDelimitedNumbers() {
        assertThat(calculator.sum("5,3,2")).isEqualTo(10);
    }

    @Test
    public void newLine() {
        assertThat(calculator.sum("5,3\n2")).isEqualTo(10);
    }

    @Test
    public void commaEndingWithNewLine() {
        try {
            calculator.sum("5,3,\n2");
            fail();
        } catch (RuntimeException e) {
            assertThat(e.getMessage()).isEqualTo("line cannot end with Delimiter");
        }
    }

    @Test
    public void nonCommaDelimiter() {
        assertThat(calculator.sum("5,3\n//;\n2;3\n5;3")).isEqualTo(21);
    }

    @Test
    public void negativeValues() {
        try {
            calculator.sum("1,3\n-2,3");
            fail("Should throw NegativeNumberException");
        } catch (NegativeNumberException e) {
            assertThat(e.getNegativeNumbers()).containsSequence(-2);
        }
    }

    @Test
    public void multipleNegativeValues() {
        try {
            calculator.sum("1,-3\n-2,3");
            fail("Should throw NegativeNumberException");
        } catch (NegativeNumberException e) {
            assertThat(e.getNegativeNumbers()).containsSequence(-3, -2);
        }
    }

    @Test
    public void fourDigitNumbers() {
        assertThat(calculator.sum("1,1000\n3")).isEqualTo(4);
    }

    @Test
    public void multiLetterDelimiter() {
        assertThat(calculator.sum("1,2\n//[---]\n3---4---5")).isEqualTo(15);
    }

    @Test
    public void multipleDelimiterInOneLine() {
        assertThat(calculator.sum("1,2\n//[-][%]\n3-4%5")).isEqualTo(15);
    }

    @Test
    public void multipleMultiLetterDelimiterInOneLine() {
        assertThat(calculator.sum("1,2\n//[--][%%]\n3--4%%5")).isEqualTo(15);
    }


}
