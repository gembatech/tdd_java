package com.tdd;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.BiConsumer;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.stream.Collector;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(JUnit4.class)
public class SummingDelimitedStringCollectorTest {

    private BiConsumer<List<List<String>>, String> accumulatorFn;
    private Collector<String, List<List<String>>, Integer> collector;
    private List<List<String>> acc;
    private Function<List<List<String>>, Integer> summaingFn;
    private BinaryOperator<List<List<String>>> combinerFn;

    @Before
    public void init() {
        collector = new SummingDelimitedStringCollector();
        accumulatorFn = collector.accumulator();
        summaingFn = collector.finisher();
        combinerFn = collector.combiner();
        acc = new ArrayList<>();
    }

    @Test
    public void seededAccumulator() {
        accumulatorFn.accept(acc, "1,2");
        assertThat(acc.get(0)).isEqualTo(Arrays.asList(",", "1,2"));
    }

    @Test
    public void accumulatorWithValue() {
        acc.add(Arrays.asList(",", "1,2"));
        accumulatorFn.accept(acc, "3,4");
        assertThat(acc.get(1)).isEqualTo(Arrays.asList(",", "3,4"));
    }

    @Test
    public void accumulatorWithDelimiterDefinition() {
        acc.add(Arrays.asList(",", "1,2"));
        accumulatorFn.accept(acc, "//;");
        assertThat(acc.get(1)).isEqualTo(Arrays.asList(";", "0"));
    }

    @Test
    public void finisherWithSingleDelimiter() {
        acc.add(Arrays.asList(",", "1,2"));
        acc.add(Arrays.asList(",", "3,4"));
        Integer result = summaingFn.apply(acc);
        assertThat(result).isEqualTo(10);
    }

    @Test
    public void finisherWithMultipleDelimiters() {
        acc.add(Arrays.asList(",", "1,2"));
        acc.add(Arrays.asList(";", "0"));
        acc.add(Arrays.asList(";", "3;4"));
        Integer result = summaingFn.apply(acc);
        assertThat(result).isEqualTo(10);
    }

    @Test(expected = NumberFormatException.class)
    public void finisherWithIncorrectDelimiter() {
        acc.add(Arrays.asList(",", "1,2"));
        acc.add(Arrays.asList(";", "0"));
        acc.add(Arrays.asList(";", "3,4"));
        summaingFn.apply(acc);
    }

    @Test
    public void combiner() {
        List<List<String>> list1 = new ArrayList<>();
        list1.add(Arrays.asList(",", "1,2"));
        List<List<String>> list2 = new ArrayList<>();
        list2.add(Arrays.asList(",", "3,4"));
        combinerFn.apply(list1, list2);
        assertThat(list1.get(0)).containsSequence(",", "1,2");
        assertThat(list1.get(1)).containsSequence(",", "3,4");
    }
}