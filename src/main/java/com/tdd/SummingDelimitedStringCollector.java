package com.tdd;

import java.util.*;
import java.util.function.BiConsumer;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collector;
import java.util.stream.Collectors;

public class SummingDelimitedStringCollector implements Collector<String, List<List<String>>, Integer> {

    @Override
    public Supplier<List<List<String>>> supplier() {
        return ArrayList::new;
    }

    @Override
    public BiConsumer<List<List<String>>, String> accumulator() {
        return (acc, line) -> {
            String delimiter = acc.isEmpty() ? "," : acc.get(acc.size() - 1).get(0);
            if (line.startsWith("//")) {
                delimiter = new Delimiter(line).getDelimiterRegExpString();
                line = "0";
            }
            acc.add(Arrays.asList(delimiter, line));
        };
    }

    @Override
    public BinaryOperator<List<List<String>>> combiner() {
        return (list1, list2) -> {
            list1.addAll(list2);
            return list1;
        };
    }

    @Override
    public Function<List<List<String>>, Integer> finisher() {
        return acc -> {
            if (partitionBySign(acc).get(true).size() > 0) {
                throw new NegativeNumberException(partitionBySign(acc).get(true));
            }
            return partitionBySign(acc).get(false).stream()
                    .filter(x -> x < 1000)
                    .mapToInt(Integer::intValue).sum();
        };
    }

    @Override
    public Set<Characteristics> characteristics() {
        return Collections.emptySet();
    }

    private Map<Boolean, List<Integer>> partitionBySign(List<List<String>> acc) {
        return acc.stream().flatMap(x -> Arrays.stream(x.get(1).split(x.get(0))))
                .map(Integer::valueOf).collect(Collectors.partitioningBy((x -> x < 0)));
    }

}
