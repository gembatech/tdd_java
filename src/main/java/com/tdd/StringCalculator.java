package com.tdd;

import java.util.Arrays;

class StringCalculator {

    Integer sum(String input) {
        if (input.isEmpty()) {
            return 0;
        }
        if (input.contains(",\n")) {
            throw new RuntimeException("line cannot end with Delimiter");
        }
        return Arrays.stream(input.split("\n")).collect(new SummingDelimitedStringCollector());
    }

}
