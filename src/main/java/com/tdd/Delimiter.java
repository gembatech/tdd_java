package com.tdd;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

class Delimiter {

    private static final Pattern SINGLE_LETTER_DELIMITER_FINDER_REGEXP = Pattern.compile("^//(\\W?)$");
    private static final Pattern MULTIPLE_SINGLE_LETTER_DELIMITER_FINDER_REGEXP = Pattern.compile("\\[(\\W+?)]");
    private String line;

    Delimiter(String line) {
        this.line = line;
    }

    String getDelimiterRegExpString() {
        if(!line.startsWith("//")) {
            throw new RuntimeException("Delimiter Definition Char Sequence \"//\" not found");
        }
        String delimiterRegExpString = singleLetterDelimiterFinder(line);
        if (delimiterRegExpString.isEmpty()) {
            delimiterRegExpString = multiLetterDelimiterFinder(line);
        }
        return delimiterRegExpString;
    }

    private String singleLetterDelimiterFinder(String line) {
        Matcher delimiterMatcher = SINGLE_LETTER_DELIMITER_FINDER_REGEXP.matcher(line);
        if (delimiterMatcher.find()) {
            return delimiterMatcher.group(1);
        }
        return "";
    }

    private String multiLetterDelimiterFinder(String line) {
        List<String> multipleDelimiterRegExp = new ArrayList<>();
        Matcher delimiterMatcher = MULTIPLE_SINGLE_LETTER_DELIMITER_FINDER_REGEXP.matcher(line);
        while (delimiterMatcher.find()) {
            multipleDelimiterRegExp.add(delimiterMatcher.group(1));
        }
        return String.join("|", multipleDelimiterRegExp);
    }
}
